from fastapi import FastAPI, Request, Form, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
import snowflake.connector
import hashlib
import pandas as pd
import json
import openai
import uvicorn
import nest_asyncio
from datetime import datetime

# Apply nest_asyncio to allow nesting of async loops
nest_asyncio.apply()

# Initialize the FastAPI app
app = FastAPI()

# Set up Jinja2 templates
templates = Jinja2Templates(directory="templates")

# OpenAI API key
openai.api_key = 'sk-proj-d1D3xUEDrWez1VChuzPzT3BlbkFJjQGflgbpdR4hc3RWS7EP'

# Snowflake Configuration
snowflake_config = {
    'user': 'servicememositegpt',
    'password': 'vq4RmY6yAg#XJKX#2gdhv',
    'account': 'dh87550.fc87156.ca-central-1',
    'host': "dh87550.ca-central-1.aws.snowflakecomputing.com",
    'port': 443,
    'warehouse': 'MEMOSITE_GPT_WAREHOUSE',
    'database': 'timely_api',
    'schema': 'timely_api',  # Replace with actual schema
    'role': 'ACCOUNTADMIN',
    'authenticator': 'username_password_mfa'
}

# Load the column descriptions from the Excel file
column_descriptions_path = r"C:\Users\suraj.r\Desktop\Memosite-GPT\GPT-enev\column_descriptions.xlsx"
column_descriptions_df = pd.read_excel(column_descriptions_path)

# Convert column descriptions to a dictionary for easy access
column_descriptions = dict(zip(column_descriptions_df['Column Name'], column_descriptions_df['Description']))

# Function to hash sensitive information
def hash_sensitive_info(value):
    if pd.isnull(value):
        return hashlib.sha256(b'').hexdigest()
    return hashlib.sha256(str(value).encode()).hexdigest()

# Function to test Snowflake connection
def test_snowflake_connection():
    try:
        conn = snowflake.connector.connect(**snowflake_config)
        cur = conn.cursor()
        cur.execute("SELECT 1")
        result = cur.fetchone()
        print("Connection successful!" if result[0] == 1 else "Connection failed.")
        return result[0] == 1
    except snowflake.connector.errors.Error as err:
        print(f"Error: {err}")
        return False
    finally:
        if 'conn' in locals():
            cur.close()
            conn.close()

# Function to create hash map from Snowflake view
def create_hash_map_from_snowflake():
    if not test_snowflake_connection():
        raise HTTPException(status_code=500, detail="Unable to connect to Snowflake.")
    try:
        connection = snowflake.connector.connect(**snowflake_config)
        cursor = connection.cursor()
        query = "SELECT DISTINCT EMPLOYEE_ID, EMPLOYEE_NAME, EMPLOYEE_EMAIL, CLIENT_NAME, PROJECT_ID, PROJECT_NAME FROM TIMELY_API.TIMELY_REPORTS;"
        cursor.execute(query)
        unique_entries = cursor.fetchall()
        columns = ['EMPLOYEE_ID', 'EMPLOYEE_NAME', 'EMPLOYEE_EMAIL', 'CLIENT_NAME', 'PROJECT_ID', 'PROJECT_NAME']
        unique_df = pd.DataFrame(unique_entries, columns=columns)
        
        # Create a hash map for unique values
        hash_map = {}
        for column in columns:
            hash_map[column] = {}
            unique_df[column + '_MASKED'] = unique_df[column].apply(hash_sensitive_info)
            hash_map[column].update(unique_df[[column, column + '_MASKED']].set_index(column + '_MASKED').to_dict()[column])

        # Store hash map in a file
        with open('hash_map.json', 'w') as f:
            json.dump(hash_map, f)

    except snowflake.connector.errors.Error as err:
        print(f"Error: {err}")
    finally:
        if 'connection' in locals():
            cursor.close()
            connection.close()

# Function to execute SQL query and fetch results
def execute_query(sql_query):
    if not test_snowflake_connection():
        raise HTTPException(status_code=500, detail="Unable to connect to Snowflake.")
    try:
        connection = snowflake.connector.connect(**snowflake_config)
        cursor = connection.cursor()
        cursor.execute(sql_query)
        results = cursor.fetchall()
        columns = [col[0] for col in cursor.description]
        return pd.DataFrame(results, columns=columns)

    except snowflake.connector.errors.Error as err:
        print(f"Error: {err}")
        return pd.DataFrame()  # Return empty DataFrame in case of error
    finally:
        if 'connection' in locals():
            cursor.close()
            connection.close()

# Function to get a response from ChatGPT with schema and hashed question
def get_sql_query_with_schema(question, start_date, end_date):
    # Load the hash map
    with open('hash_map.json', 'r') as file:
        hash_map = json.load(file)

    # Hash the question inputs based on the hash map
    hashed_question = question
    for column, mappings in hash_map.items():
        for original_value, hashed_value in mappings.items():
            if original_value in question:
                hashed_question = hashed_question.replace(original_value, hashed_value)

    # Add column descriptions to the prompt
    column_context = "\n".join([f"{col}: {desc}" for col, desc in column_descriptions.items()])
    prompt = f"Question: {hashed_question}\nStart Date: {start_date}\nEnd Date: {end_date}\nColumn Descriptions:\n{column_context}"
    print(prompt)

    # Prepare messages for ChatGPT
    messages = [
        {"role": "system", "content": "You are a database assistant. Provide Snowflake queries based on the given data, schema, and criteria."},
        {"role": "user", "content": prompt}
    ]

    try:
        # Call the OpenAI API
        response = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=messages,
            max_tokens=150,
            temperature=0.8
        )
        raw_response_text = response.choices[0].message.content.strip()
        print(raw_response_text)
        return raw_response_text  # Return the SQL query for execution

    except Exception as e:
        print(f"Exception: {e}")
        return "An error occurred while calling the OpenAI API."

# Route for the home page (HTML Form)
@app.get("/", response_class=HTMLResponse)
async def home(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

# Route to handle form submission
from fastapi.responses import JSONResponse

@app.post("/ask")
async def ask_question(request: Request, question: str = Form(...), start_date: str = Form(...), end_date: str = Form(...)):
    # Create hash map before processing the question (if not done already)
    create_hash_map_from_snowflake()

    # Process the question and get the SQL query
    sql_query = get_sql_query_with_schema(question, start_date, end_date)

    # Clean the query and remove unwanted formatting characters
    clean_query = sql_query.replace('`', '').replace('your_table_name', 'TIMELY_API.TIMELY_REPORTS')

    # Remove extra spaces and any unexpected characters
    clean_query = clean_query.strip()  # Ensure no leading/trailing spaces
    clean_query = " ".join(clean_query.split())  # Collapse multiple spaces into one

    # Remove any unwanted additional text like 'Connection successful!'
    if "Connection successful!" in clean_query:
        clean_query = clean_query.replace("Connection successful!", "").strip()

    # Specifically check and remove any leading 'sql' keyword or other unexpected characters
    if clean_query.lower().startswith('sql'):
        clean_query = clean_query[3:].strip()  # Remove the 'sql' part at the start

    print(f"Clean query: {clean_query}")

    # Execute the SQL query and get results
    result_df = execute_query(clean_query)

    # Check if the result is empty or an error occurred
    if result_df.empty:
        result_html = "<p>An error occurred while fetching the data. Please try again later.</p>"
        return JSONResponse(content={"message": result_html}, status_code=500)
    else:
        # Convert results to a dictionary for better handling in the frontend
        result_data = result_df.to_dict(orient="records")
        return JSONResponse(content={"result": result_data}, status_code=200)



# Run the FastAPI app
if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
